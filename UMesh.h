#pragma once

#include <string>
#include <mutex>
#include <stdint.h>
#include <memory>
#include <iostream>
#include <array>
#include <vector>
#include <limits>
#include <algorithm>
#include <fstream>
#include <string>

namespace UMeshLoader {

const size_t bum_magic = 0x234235566ULL;

#ifdef __WIN32__
#define _snprintf sprintf_s
#else
#define _snprintf snprintf
#endif

#ifndef PRINT
#define PRINT(var) std::cout << #var << "=" << var << std::endl;
#define PING std::cout << __FILE__ << "::" << __LINE__ << ": " << __FUNCTION__ << std::endl;
#endif

inline std::string prettyNumber(const size_t s)
{
    char buf[1000];
    if (s >= (1024LL * 1024LL * 1024LL * 1024LL))
    {
        _snprintf(buf, 1000, "%.2fT", s / (1024.f * 1024.f * 1024.f * 1024.f));
    }
    else if (s >= (1024LL * 1024LL * 1024LL))
    {
        _snprintf(buf, 1000, "%.2fG", s / (1024.f * 1024.f * 1024.f));
    }
    else if (s >= (1024LL * 1024LL))
    {
        _snprintf(buf, 1000, "%.2fM", s / (1024.f * 1024.f));
    }
    else if (s >= (1024LL))
    {
        _snprintf(buf, 1000, "%.2fK", s / (1024.f));
    }
    else
    {
        _snprintf(buf, 1000, "%zi", s);
    }
    return buf;
}

struct Tet
{
    uint32_t indices[4];
    void set_indices(std::array<uint32_t, 4> indices) {
        this->indices[0] = indices[0];
        this->indices[1] = indices[1];
        this->indices[2] = indices[2];
        this->indices[3] = indices[3];
    }
    std::array<uint32_t,4> get_indices() {
        return {indices[0],indices[1],indices[2],indices[3]};
    }
};

struct Wedge
{
    uint32_t base[3];
    uint32_t top[3];
    void set_base(std::array<uint32_t, 3> base) {
        this->base[0] = base[0];
        this->base[1] = base[1];
        this->base[2] = base[2];
    }
    void set_top(std::array<uint32_t, 3> top) {
        this->top[0] = top[0];
        this->top[1] = top[1];
        this->top[2] = top[2];
    }
    std::array<uint32_t,3> get_base() {
        return {base[0],base[1],base[2]};
    }
    std::array<uint32_t,3> get_top() {
        return {top[0],top[1],top[2]};
    }
};

struct Hex
{
    uint32_t base[4];
    uint32_t top[4];
    void set_base(std::array<uint32_t, 4> base) {
        this->base[0] = base[0];
        this->base[1] = base[1];
        this->base[2] = base[2];
        this->base[3] = base[3];
    }
    void set_top(std::array<uint32_t, 4> top) {
        this->top[0] = top[0];
        this->top[1] = top[1];
        this->top[2] = top[2];
        this->top[3] = top[3];
    }
    std::array<uint32_t,4> get_base() {
        return {base[0],base[1],base[2],base[3]};
    }
    std::array<uint32_t,4> get_top() {
        return {top[0],top[1],top[2],top[3]};
    }
};

struct Pyr
{
    uint32_t base[4];
    uint32_t top;
    void set_base(std::array<uint32_t, 4> base) {
        this->base[0] = base[0];
        this->base[1] = base[1];
        this->base[2] = base[2];
        this->base[3] = base[3];
    }
    void set_top(uint32_t top) {
        this->top = top;
    }
    std::array<uint32_t,4> get_base() {
        return {base[0],base[1],base[2],base[3]};
    }
    uint32_t get_top() {
        return top;
    }
};

struct Attribute
{
    std::vector<float> values;
    float valueRange[2];

    Attribute(int num=0) : values(num) {}

    void finalize()
    {
        valueRange[0] = std::numeric_limits<float>::max();
        valueRange[1] = std::numeric_limits<float>::min();
        for (auto &v : values)
        {
            valueRange[0] = std::min(valueRange[0], v);
            valueRange[1] = std::max(valueRange[1], v);
        };
    }
};

struct Bounds
{
    float lower[3];
    float upper[3];
};

inline Bounds createBounds()
{
    return {
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::min(),
        std::numeric_limits<float>::min(),
        std::numeric_limits<float>::min(),
    };
}

inline Bounds extendBounds(Bounds b, std::array<float, 3> v) {
    b.lower[0] = std::min(b.lower[0], v[0]);
    b.lower[1] = std::min(b.lower[1], v[1]);
    b.lower[2] = std::min(b.lower[2], v[2]);
    b.upper[0] = std::max(b.upper[0], v[0]);
    b.upper[1] = std::max(b.upper[1], v[1]);
    b.upper[2] = std::max(b.upper[2], v[2]);
    return b;
};

struct Bounds4F
{
    float lower[4];
    float upper[4];
};

typedef enum
{
    TRI,
    QUAD,
    TET,
    PYR,
    WEDGE,
    HEX
} PrimType;

struct PrimRef
{
    inline PrimRef() {}
    inline PrimRef(PrimType type, size_t ID) : type(type), ID(ID) {}
    inline PrimRef(const PrimRef &) = default;
    inline bool isTet() const { return type == TET; }
    union {
        struct
        {
            size_t type : 4;
            size_t ID : 60;
        };
        size_t as_size_t;
    };
};

struct UMesh
{
    // typedef std::shared_ptr<UMesh> SP;
    std::vector<std::array<float, 3>> vertices;
    std::shared_ptr<Attribute> perVertex;
    // std::shared_ptr<Attribute> perTri;
    // std::shared_ptr<Attribute> perQuad;
    // std::shared_ptr<Attribute> perTet;
    // std::shared_ptr<Attribute> perPyr;
    // std::shared_ptr<Attribute> perWedge;
    // std::shared_ptr<Attribute> perHex;

    // -------------------------------------------------------
    // surface elements:
    // -------------------------------------------------------
    std::vector<std::array<uint32_t, 3>> triangles;
    std::vector<std::array<uint32_t, 4>> quads;

    // -------------------------------------------------------
    // volume elements:
    // -------------------------------------------------------
    std::vector<Tet> tets;
    std::vector<Pyr> pyrs;
    std::vector<Wedge> wedges;
    std::vector<Hex> hexes;

    Bounds bounds;

    UMesh() {
        perVertex = std::make_shared<Attribute>();
    }

    inline void print()
    {
        std::cout << "#verts            : " << prettyNumber(vertices.size()) << std::endl;
        std::cout << "#tris             : " << prettyNumber(triangles.size()) << std::endl;
        std::cout << "#quads            : " << prettyNumber(quads.size()) << std::endl;
        std::cout << "#tets             : " << prettyNumber(tets.size()) << std::endl;
        std::cout << "#pyrs             : " << prettyNumber(pyrs.size()) << std::endl;
        std::cout << "#wedges           : " << prettyNumber(wedges.size()) << std::endl;
        std::cout << "#hexes            : " << prettyNumber(hexes.size()) << std::endl;
    }

    inline std::string toString()
    {
        return std::string("#verts            : ") + prettyNumber(vertices.size()) + std::string("\n")
             + std::string("#tris             : ") + prettyNumber(triangles.size()) + std::string("\n")
             + std::string("#quads            : ") + prettyNumber(quads.size()) + std::string("\n")
             + std::string("#tets             : ") + prettyNumber(tets.size()) + std::string("\n")
             + std::string("#pyrs             : ") + prettyNumber(pyrs.size()) + std::string("\n")
             + std::string("#wedges           : ") + prettyNumber(wedges.size()) + std::string("\n")
             + std::string("#hexes            : ") + prettyNumber(hexes.size()) + std::string("\n");  
    }

    inline void createVolumePrimRefs(std::vector<PrimRef> &result)
    {
#if 0
        result.resize(tets.size()+pyrs.size()+wedges.size()+hexes.size());
        parallel_for_blocked(0ull,tets.size(),64*1024,[&](size_t begin,size_t end){
            for (size_t i=begin;i<end;i++)
            result[i] = PrimRef(TET,i);
        });
        parallel_for_blocked(0ull,pyrs.size(),64*1024,[&](size_t begin,size_t end){
            for (size_t i=begin;i<end;i++)
            result[tets.size()+i] = PrimRef(PYR,i);
        });
        parallel_for_blocked(0ull,wedges.size(),64*1024,[&](size_t begin,size_t end){
            for (size_t i=begin;i<end;i++)
            result[tets.size()+pyrs.size()+i] = PrimRef(WEDGE,i);
        });
        parallel_for_blocked(0ull,hexes.size(),64*1024,[&](size_t begin,size_t end){
            for (size_t i=begin;i<end;i++)
            result[tets.size()+pyrs.size()+wedges.size()+i] = PrimRef(HEX,i);
        });
#else
        for (int i = 0; i < tets.size(); i++)
            // if (area(getBounds(PrimRef(TET,i))) > 0.f)
            result.push_back(PrimRef(TET, i));

        for (int i = 0; i < pyrs.size(); i++)
            // if (area(getBounds(PrimRef(PYR,i))) > 0.f)
            result.push_back(PrimRef(PYR, i));

        for (int i = 0; i < wedges.size(); i++)
            // if (area(getBounds(PrimRef(WEDGE,i))) > 0.f)
            result.push_back(PrimRef(WEDGE, i));

        for (int i = 0; i < hexes.size(); i++)
            // if (area(getBounds(PrimRef(HEX,i))) > 0.f)
            result.push_back(PrimRef(HEX, i));
#endif
        PRINT(prettyNumber(tets.size()));
        PRINT(prettyNumber(pyrs.size()));
        PRINT(prettyNumber(wedges.size()));
        PRINT(prettyNumber(hexes.size()));
    }

    inline void createSurfacePrimRefs(std::vector<PrimRef> &result)
    {
        throw std::runtime_error("not implemented..");
    }

    inline size_t size() const
    {
        return triangles.size() +
               quads.size() +
               hexes.size() +
               tets.size() +
               wedges.size() +
               pyrs.size();
    }

    inline std::array<float, 2> getValueRange() const
    {
        return {perVertex->valueRange[0], perVertex->valueRange[1]};
    }

    inline Bounds getBounds() const
    {
        return bounds;
    }

    inline Bounds4F getBounds4f() const
    {
        return {
            {bounds.lower[0], bounds.lower[1], bounds.lower[2], perVertex->valueRange[0]},
            {bounds.upper[0], bounds.upper[1], bounds.upper[2], perVertex->valueRange[1]}};
    }

    inline std::array<float, 2> getTetValueRange(const size_t ID) const
    {
        std::vector<float> values = {
            perVertex->values[tets[ID].indices[0]],
            perVertex->values[tets[ID].indices[1]],
            perVertex->values[tets[ID].indices[2]],
            perVertex->values[tets[ID].indices[3]],
        };

        const std::array<float, 2> b = {
            *std::min_element(values.begin(), values.end()),
            *std::max_element(values.begin(), values.end()),
        };

        return b;
    }

    inline std::array<float, 2> getPyrValueRange(const size_t ID) const
    {
        std::vector<float> values = {
            perVertex->values[pyrs[ID].top],
            perVertex->values[pyrs[ID].base[0]],
            perVertex->values[pyrs[ID].base[1]],
            perVertex->values[pyrs[ID].base[2]],
            perVertex->values[pyrs[ID].base[3]]};

        const std::array<float, 2> b = {
            *std::min_element(values.begin(), values.end()),
            *std::max_element(values.begin(), values.end()),
        };

        return b;
    }

    inline std::array<float, 2> getWedgeValueRange(const size_t ID) const
    {
        std::vector<float> values = {
            perVertex->values[wedges[ID].top[0]],
            perVertex->values[wedges[ID].top[1]],
            perVertex->values[wedges[ID].base[0]],
            perVertex->values[wedges[ID].base[1]],
            perVertex->values[wedges[ID].base[2]],
            perVertex->values[wedges[ID].base[3]],
        };

        const std::array<float, 2> b = {
            *std::min_element(values.begin(), values.end()),
            *std::max_element(values.begin(), values.end()),
        };

        return b;
    }

    inline std::array<float, 2> getHexValueRange(const size_t ID) const
    {
        std::vector<float> values = {
            perVertex->values[hexes[ID].top[0]],
            perVertex->values[hexes[ID].top[1]],
            perVertex->values[hexes[ID].top[2]],
            perVertex->values[hexes[ID].top[3]],
            perVertex->values[hexes[ID].base[0]],
            perVertex->values[hexes[ID].base[1]],
            perVertex->values[hexes[ID].base[2]],
            perVertex->values[hexes[ID].base[3]]};

        const std::array<float, 2> b = {
            *std::min_element(values.begin(), values.end()),
            *std::max_element(values.begin(), values.end()),
        };

        return b;
    }

    inline std::array<float, 2> getValueRange(const PrimRef &pr) const
    {
        switch (pr.type)
        {
        case TET:
            return getTetValueRange(pr.ID);
        case PYR:
            return getPyrValueRange(pr.ID);
        case WEDGE:
            return getWedgeValueRange(pr.ID);
        case HEX:
            return getHexValueRange(pr.ID);
        default:
            throw std::runtime_error("not implemented");
        };
    }

    inline std::array<float, 6> getTetBounds(const size_t ID) const
    {
        std::vector<float> x_values = {
            vertices[tets[ID].indices[0]][0],
            vertices[tets[ID].indices[1]][0],
            vertices[tets[ID].indices[2]][0],
            vertices[tets[ID].indices[3]][0]};

        std::vector<float> y_values = {
            vertices[tets[ID].indices[0]][1],
            vertices[tets[ID].indices[1]][1],
            vertices[tets[ID].indices[2]][1],
            vertices[tets[ID].indices[3]][1]};

        std::vector<float> z_values = {
            vertices[tets[ID].indices[0]][2],
            vertices[tets[ID].indices[1]][2],
            vertices[tets[ID].indices[2]][2],
            vertices[tets[ID].indices[3]][2]};

        const std::array<float, 6> b = {
            *std::min_element(x_values.begin(), x_values.end()),
            *std::min_element(y_values.begin(), y_values.end()),
            *std::min_element(z_values.begin(), z_values.end()),
            *std::max_element(x_values.begin(), x_values.end()),
            *std::max_element(y_values.begin(), y_values.end()),
            *std::max_element(z_values.begin(), z_values.end()),
        };

        return b;
    }

    inline std::array<float, 6> getPyrBounds(const size_t ID) const
    {
        std::vector<float> x_values = {
            vertices[pyrs[ID].top][0],
            vertices[pyrs[ID].base[0]][0],
            vertices[pyrs[ID].base[1]][0],
            vertices[pyrs[ID].base[2]][0],
            vertices[pyrs[ID].base[3]][0]};
        std::vector<float> y_values = {
            vertices[pyrs[ID].top][1],
            vertices[pyrs[ID].base[0]][1],
            vertices[pyrs[ID].base[1]][1],
            vertices[pyrs[ID].base[2]][1],
            vertices[pyrs[ID].base[3]][1]};
        std::vector<float> z_values = {
            vertices[pyrs[ID].top][2],
            vertices[pyrs[ID].base[0]][2],
            vertices[pyrs[ID].base[1]][2],
            vertices[pyrs[ID].base[2]][2],
            vertices[pyrs[ID].base[3]][2]};

        const std::array<float, 6> b = {
            *std::min_element(x_values.begin(), x_values.end()),
            *std::min_element(y_values.begin(), y_values.end()),
            *std::min_element(z_values.begin(), z_values.end()),
            *std::max_element(x_values.begin(), x_values.end()),
            *std::max_element(y_values.begin(), y_values.end()),
            *std::max_element(z_values.begin(), z_values.end()),
        };

        return b;
    }

    inline std::array<float, 6> getWedgeBounds(const size_t ID) const
    {
        std::vector<float> x_values = {
            vertices[wedges[ID].top[0]][0],
            vertices[wedges[ID].top[1]][0],
            vertices[wedges[ID].base[0]][0],
            vertices[wedges[ID].base[1]][0],
            vertices[wedges[ID].base[2]][0],
            vertices[wedges[ID].base[3]][0],
        };
        std::vector<float> y_values = {
            vertices[wedges[ID].top[0]][1],
            vertices[wedges[ID].top[1]][1],
            vertices[wedges[ID].base[0]][1],
            vertices[wedges[ID].base[1]][1],
            vertices[wedges[ID].base[2]][1],
            vertices[wedges[ID].base[3]][1],
        };
        std::vector<float> z_values = {
            vertices[wedges[ID].top[0]][2],
            vertices[wedges[ID].top[1]][2],
            vertices[wedges[ID].base[0]][2],
            vertices[wedges[ID].base[1]][2],
            vertices[wedges[ID].base[2]][2],
            vertices[wedges[ID].base[3]][2],
        };

        const std::array<float, 6> b = {
            *std::min_element(x_values.begin(), x_values.end()),
            *std::min_element(y_values.begin(), y_values.end()),
            *std::min_element(z_values.begin(), z_values.end()),
            *std::max_element(x_values.begin(), x_values.end()),
            *std::max_element(y_values.begin(), y_values.end()),
            *std::max_element(z_values.begin(), z_values.end()),
        };

        return b;
    }

    inline std::array<float, 6> getHexBounds(const size_t ID) const
    {
        std::vector<float> x_values = {
            vertices[hexes[ID].top[0]][0],
            vertices[hexes[ID].top[1]][0],
            vertices[hexes[ID].top[2]][0],
            vertices[hexes[ID].top[3]][0],
            vertices[hexes[ID].base[0]][0],
            vertices[hexes[ID].base[1]][0],
            vertices[hexes[ID].base[2]][0],
            vertices[hexes[ID].base[3]][0],
        };
        std::vector<float> y_values = {
            vertices[hexes[ID].top[0]][1],
            vertices[hexes[ID].top[1]][1],
            vertices[hexes[ID].top[2]][1],
            vertices[hexes[ID].top[3]][1],
            vertices[hexes[ID].base[0]][1],
            vertices[hexes[ID].base[1]][1],
            vertices[hexes[ID].base[2]][1],
            vertices[hexes[ID].base[3]][1],
        };
        std::vector<float> z_values = {
            vertices[hexes[ID].top[0]][2],
            vertices[hexes[ID].top[1]][2],
            vertices[hexes[ID].top[2]][2],
            vertices[hexes[ID].top[3]][2],
            vertices[hexes[ID].base[0]][2],
            vertices[hexes[ID].base[1]][2],
            vertices[hexes[ID].base[2]][2],
            vertices[hexes[ID].base[3]][2],
        };

        const std::array<float, 6> b = {
            *std::min_element(x_values.begin(), x_values.end()),
            *std::min_element(y_values.begin(), y_values.end()),
            *std::min_element(z_values.begin(), z_values.end()),
            *std::max_element(x_values.begin(), x_values.end()),
            *std::max_element(y_values.begin(), y_values.end()),
            *std::max_element(z_values.begin(), z_values.end()),
        };

        return b;
    }

    inline std::array<float, 6> getBounds(const PrimRef &pr) const
    {
        switch (pr.type)
        {
        case TET:
            return getTetBounds(pr.ID);
        case PYR:
            return getPyrBounds(pr.ID);
        case WEDGE:
            return getWedgeBounds(pr.ID);
        case HEX:
            return getHexBounds(pr.ID);
        default:
            throw std::runtime_error("not implemented");
        };
        // return bounds;
    }

    inline std::array<float, 8> getBounds4f(const PrimRef &pr) const
    {
        auto spatial = getBounds(pr);
        auto value = getValueRange(pr);

        return {
            spatial[0],
            spatial[1],
            spatial[2],
            value[0],
            spatial[3],
            spatial[4],
            spatial[5],
            value[1],
        };
    }

    void finalize()
    {
        perVertex->finalize();
        bounds = createBounds();
#if 0
        std::mutex mutex;
        parallel_for_blocked(0, vertices.size(), 16 * 1024, [&](size_t begin, size_t end) {
            box3f rangeBounds;
            for (size_t i = begin; i < end; i++)
                rangeBounds.extend(vertices[i]);
            std::lock_guard<std::mutex> lock(mutex);
            bounds.extend(rangeBounds);
        });
#else
        for (auto v : vertices)
            bounds = extendBounds(bounds, v);
#endif
    }

    // /*! if data set has per-vertex data this won't change anything; if
    //     it has per-cell scalar values it will compute create a new
    //     per-vertex data field based on the averages of all adjacent
    //     cell scalars */
    // void createPerVertexData()
    // {
    //     std::cout << "=======================================================" << std::endl;
    //     PING;

    //     if (perVertex)
    //         return;

    //     if (!perTet && !perHex)
    //         throw std::runtime_error("cannot generate per-vertex interpolated scalar field: data set has neither per-cell nor per-vertex data fields assigned!?");

    //     PRINT(vertex.size());
    //     this->perVertex = std::make_shared<Attribute>(vertex.size());

    //     struct Helper
    //     {
    //         Helper(std::shared_ptr<Attribute> perVertex)
    //             : perVertex(perVertex),
    //             renormalize_weights(perVertex->value.size())
    //         {
    //             assert(perVertex);
    //         }
    //         ~Helper()
    //         {
    //             for (int i = 0; i < renormalize_weights.size(); i++)
    //                 if (renormalize_weights[i] > 0)
    //                     perVertex->values[i] *= 1.f / renormalize_weights[i];
    //             perVertex->finalize();
    //             PRINT(std::string(perVertex->valueRange.begin(), perVertex->valueRange.end()));
    //         }

    //         void splat(float value, int vertexID)
    //         {
    //             perVertex->values[vertexID] += value;
    //             renormalize_weights[vertexID] += 1.f;
    //         }
    //         std::shared_ptr<Attribute> perVertex;
    //         std::vector<float> renormalize_weights;
    //     } helper(this->perVertex);

    //     assert(pyrs.empty());   // not implemented
    //     assert(wedges.empty()); // not implemented
    //     for (int i = 0; i < tets.size(); i++)
    //     {
    //         helper.splat(perTet->values[i], tets[i].indices[0]);
    //         helper.splat(perTet->values[i], tets[i].indices[1]);
    //         helper.splat(perTet->values[i], tets[i].indices[2]);
    //         helper.splat(perTet->values[i], tets[i].indices[3]);
    //     }
    //     for (int i = 0; i < hexes.size(); i++)
    //     {
    //         helper.splat(perHex->values[i], hexes[i].base[0]);
    //         helper.splat(perHex->values[i], hexes[i].base[1]);
    //         helper.splat(perHex->values[i], hexes[i].base[2]);
    //         helper.splat(perHex->values[i], hexes[i].base[3]);
    //         helper.splat(perHex->values[i], hexes[i].top[0]);
    //         helper.splat(perHex->values[i], hexes[i].top[1]);
    //         helper.splat(perHex->values[i], hexes[i].top[2]);
    //         helper.splat(perHex->values[i], hexes[i].top[3]);
    //     }

    //     // aaaaand .... free the old per-cell data
    //     this->perTet = nullptr;
    //     this->perHex = nullptr;
    // }
};

inline std::shared_ptr<UMesh> CreateUMesh()
{
    return std::make_shared<UMesh>();
}

template <typename T>
void writeElement(std::ofstream &out, const T &t)
{
    out.write((char *)&t, sizeof(t));
    assert(out.good());
}

template <typename T>
void writeVector(std::ofstream &out, const std::vector<T> &vt)
{
    size_t N = vt.size();
    writeElement(out, N);
    for (auto &v : vt)
        writeElement(out, v);
    // if (out.fail()) std::cout<<"Error: Logical error on i/o operation"<<std::endl;
    // if (out.bad()) std::cout<<"Error: Read/writing error on i/o operation"<<std::endl;
    assert(out.good());
}

template <typename T>
void readElement(std::ifstream &in, T &t)
{
    assert(in.good());
    in.read((char *)&t, sizeof(t));
}

template <typename T>
void readArray(std::ifstream &in, T *t, size_t N)
{
    assert(in.good());
    for (size_t i = 0; i < N; i++)
        readElement(in, t[i]);
}

template <typename T>
void readVector(std::ifstream &in, std::vector<T> &t, const std::string &description)
{
    size_t N;
    readElement(in, N);
    t.resize(N);
    for (size_t i = 0; i < N; i++)
        readElement(in, t[i]);
}

inline void saveBinaryUMesh(const std::string &fileName, std::shared_ptr<UMesh> mesh)
{
    std::ofstream out(fileName, std::ios_base::binary);
    writeElement(out, bum_magic);
    writeVector(out, mesh->vertices);
    writeVector(out, mesh->perVertex->values);
    writeVector(out, mesh->triangles);
    writeVector(out, mesh->quads);
    writeVector(out, mesh->tets);
    writeVector(out, mesh->pyrs);
    writeVector(out, mesh->wedges);
    writeVector(out, mesh->hexes);
    out.close();
    return;
}

inline std::shared_ptr<UMesh> loadBinaryUMesh(const std::string &fileName)
{
    auto mesh = std::make_shared<UMesh>();
    std::ifstream in(fileName, std::ios_base::binary);
    size_t magic;
    readElement(in, magic);
    if (magic != bum_magic)
        throw std::runtime_error("wrong magic number in umesh file ...");
    readVector(in, mesh->vertices, "vertices");
    mesh->perVertex = std::make_shared<Attribute>();
    readVector(in, mesh->perVertex->values, "scalars");
    mesh->perVertex->finalize();
    readVector(in, mesh->triangles, "triangles");
    readVector(in, mesh->quads, "quads");
    readVector(in, mesh->tets, "tets");
    readVector(in, mesh->pyrs, "pyramids");
    readVector(in, mesh->wedges, "wedges");
    readVector(in, mesh->hexes, "hexes");
    mesh->finalize();
    return mesh;
}

};
